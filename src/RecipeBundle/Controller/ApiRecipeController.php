<?php

namespace RecipeBundle\Controller;

use RecipeBundle\Entity\Recipe;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use FOS\RestBundle\Controller\FOSRestController;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;

use Symfony\Component\HttpKernel\Exception\HttpException;
use Symfony\Component\HttpFoundation\Request;


class ApiRecipeController extends Controller
{
    /**
     * Get recipe by id
     *
     * @ApiDoc(
     *  section = "Recipe",
     *  resource = true,
     *  description="Get recipe by id",
     *  requirements={
     *      {
     *          "name"="id",
     *          "dataType"="integer",
     *          "requirement"="\d+",
     *          "description"="Id of recipe"
     *      }
     *  }
     * )
     */
    public function getRecipeAction($id){
        $recipeService = $this->get('recipe.service');

        return $recipeService->getById($id);
    }


    /**
     * Fetch all recipes for a specific cuisine with pagination
     *
     * @ApiDoc(
     *  section = "Recipe",
     *  resource = true,
     *  description="Fetch all recipes for a specific cuisine with pagination",
     *      requirements={
     *          { "name"="cuisine", "dataType"="string", "requirement"="\w+", "description"="Name of the cuisine"},
     *          { "name"="pageNumber", "dataType"="integer", "required"=false, "requirement"="\d+","description"="Number of current page"},
     *          { "name"="limitPerPage", "dataType"="integer", "required"=true, "requirement"="\d+", "description"="Number of objects to return"}
     *      }
     * )
     */
    public function getRecipeByCuisineAction($cuisine, $pageNumber, $limitPerPage){
        $recipeService  = $this->get('recipe.service');
        $paginator      = $this->get('knp_paginator');

        $extraParams = array(
            'findBy' => array(
                        'recipeCuisine'=>$cuisine
                    )
        );
        $recipeQuery = $recipeService->getRecipePaginatorQuery($extraParams);
        $pagination = $paginator->paginate(
            $recipeQuery,
            $pageNumber,
            $limitPerPage,
            ['distinct' => true]
        );
        return $pagination;
    }

    /**
     * Rate recipe by id
     *
     * @ApiDoc(
     *  section = "Recipe",
     *  resource = true,
     *  description="Rate recipe by id",
     *  requirements={
     *      {
     *          "name"="id",
     *          "dataType"="integer",
     *          "requirement"="\d+",
     *          "description"="Id of recipe"
     *      },
     *     {
     *          "name"="rateValue",
     *          "dataType"="integer",
     *          "requirement"="\d+",
     *          "description"="Value rate of recipe"
     *      }
     *  }
     * )
     */
    public function putRecipeRateAction($id, $rateValue){
        $recipeService = $this->get('recipe.service');

        if($rateValue < 1 || $rateValue > 5){
            throw new HttpException(400, "Rate Value {$rateValue} has to be between 1 and 5");
        }

        $recipeInfo = $recipeService->getById($id);
        $recipeInfo->setRate($rateValue);

        $recipeService->saveRecipe($recipeInfo);

        return $recipeInfo;
    }

    /**
     * Update recipe by id
     *
     * @ApiDoc(
     *  section = "Recipe",
     *  resource = true,
     *  description="Update recipe by id",
     *  requirements={
     *      {
     *          "name"="boxType",
     *          "dataType"="string"
     *      },
     *     {
     *          "name"="title",
     *          "dataType"="string"
     *      },
     *     {
     *          "name"="slug",
     *          "dataType"="string"
     *      },
     *     {
     *          "name"="shortTitle",
     *          "dataType"="string"
     *      },
     *     {
     *          "name"="marketingDescription",
     *          "dataType"="string"
     *      },
     *
     *
     *     {
     *          "name"="caloriesKcal",
     *          "dataType"="integer"
     *      },
     *     {
     *          "name"="proteinGrams",
     *          "dataType"="integer"
     *      },
     *     {
     *          "name"="fatGrams",
     *          "dataType"="integer"
     *      },
     *     {
     *          "name"="carbsGrams",
     *          "dataType"="integer"
     *      },
     *     {
     *          "name"="bulletpoint1",
     *          "dataType"="string"
     *      },
     *
     *
     *     {
     *          "name"="bulletpoint2",
     *          "dataType"="string"
     *      },
     *     {
     *          "name"="bulletpoint3",
     *          "dataType"="string"
     *      },
     *     {
     *          "name"="recipeDietTypeId",
     *          "dataType"="string"
     *      },
     *     {
     *          "name"="season",
     *          "dataType"="string"
     *      },
     *     {
     *          "name"="base",
     *          "dataType"="string"
     *      },
     *
     *     {
     *          "name"="proteinSource",
     *          "dataType"="string"
     *      },
     *     {
     *          "name"="preparationTimeMinutes",
     *          "dataType"="integer"
     *      },
     *     {
     *          "name"="shelfLifeDays",
     *          "dataType"="integer"
     *      },
     *     {
     *          "name"="equipmentNeeded",
     *          "dataType"="string"
     *      },
     *     {
     *          "name"="originCountry",
     *          "dataType"="string"
     *      },
     *
     *     {
     *          "name"="recipeCuisine",
     *          "dataType"="string"
     *      },
     *     {
     *          "name"="inYourBox",
     *          "dataType"="string"
     *      },
     *     {
     *          "name"="goustoReference",
     *          "dataType"="integer"
     *      },
     *     {
     *          "name"="rate",
     *          "dataType"="integer"
     *      }
     *
     *  }
     * )
     */
    public function putRecipeAction(Request $request, $id){
        $recipeService = $this->get('recipe.service');
        $recipeInfo = $recipeService->getById($id);

        $boxType = !empty($request->get('boxType')) ? $request->get('boxType') : NULL;
        if(!empty($boxType)){
            $recipeInfo->setBoxType($boxType);
        }
        $title = !empty($request->get('title')) ? $request->get('title') : NULL;
        if(!empty($title)){
            $recipeInfo->setTitle($title);
        }
        $slug = !empty($request->get('slug')) ? $request->get('slug') : NULL;
        if(!empty($slug)){
            $recipeInfo->setSlug($slug);
        }
        $shortTitle = !empty($request->get('shortTitle')) ? $request->get('shortTitle') : NULL;
        if(!empty($shortTitle)){
            $recipeInfo->setShortTitle($shortTitle);
        }
        $marketingDescription = !empty($request->get('marketingDescription')) ? $request->get('marketingDescription') : NULL;
        if(!empty($marketingDescription)){
            $recipeInfo->setMarketingDescription($marketingDescription);
        }


        $caloriesKcal = !empty($request->get('caloriesKcal')) ? $request->get('caloriesKcal') : NULL;
        if(!empty($caloriesKcal)){
            $recipeInfo->setCaloriesKcal($caloriesKcal);
        }
        $proteinGrams = !empty($request->get('proteinGrams')) ? $request->get('proteinGrams') : NULL;
        if(!empty($proteinGrams)){
            $recipeInfo->setProteinGrams($proteinGrams);
        }
        $fatGrams = !empty($request->get('fatGrams')) ? $request->get('fatGrams') : NULL;
        if(!empty($fatGrams)){
            $recipeInfo->setFatGrams($fatGrams);
        }
        $carbsGrams = !empty($request->get('carbsGrams')) ? $request->get('carbsGrams') : NULL;
        if(!empty($carbsGrams)){
            $recipeInfo->setCarbsGrams($carbsGrams);
        }
        $bulletpoint1 = !empty($request->get('bulletpoint1')) ? $request->get('bulletpoint1') : NULL;
        if(!empty($slug)){
            $recipeInfo->setBulletpoint1($bulletpoint1);
        }


        $bulletpoint2 = !empty($request->get('bulletpoint2')) ? $request->get('bulletpoint2') : NULL;
        if(!empty($bulletpoint2)){
            $recipeInfo->setBulletpoint2($bulletpoint2);
        }
        $bulletpoint3 = !empty($request->get('bulletpoint3')) ? $request->get('bulletpoint3') : NULL;
        if(!empty($bulletpoint3)){
            $recipeInfo->setBulletpoint3($bulletpoint3);
        }
        $recipeDietTypeId = !empty($request->get('recipeDietTypeId')) ? $request->get('recipeDietTypeId') : NULL;
        if(!empty($recipeDietTypeId)){
            $recipeInfo->setRecipeDietTypeId($recipeDietTypeId);
        }
        $season = !empty($request->get('season')) ? $request->get('season') : NULL;
        if(!empty($season)){
            $recipeInfo->setSeason($season);
        }
        $base = !empty($request->get('base')) ? $request->get('base') : NULL;
        if(!empty($base)){
            $recipeInfo->setBase($base);
        }


        $proteinSource = !empty($request->get('proteinSource')) ? $request->get('proteinSource') : NULL;
        if(!empty($proteinSource)){
            $recipeInfo->setProteinSource($proteinSource);
        }
        $preparationTimeMinutes = !empty($request->get('preparationTimeMinutes')) ? $request->get('preparationTimeMinutes') : NULL;
        if(!empty($preparationTimeMinutes)){
            $recipeInfo->setPreparationTimeMinutes($preparationTimeMinutes);
        }
        $shelfLifeDays = !empty($request->get('shelfLifeDays')) ? $request->get('shelfLifeDays') : NULL;
        if(!empty($shelfLifeDays)){
            $recipeInfo->setShelfLifeDays($shelfLifeDays);
        }
        $equipmentNeeded = !empty($request->get('equipmentNeeded')) ? $request->get('equipmentNeeded') : NULL;
        if(!empty($equipmentNeeded)){
            $recipeInfo->setEquipmentNeeded($equipmentNeeded);
        }
        $originCountry = !empty($request->get('originCountry')) ? $request->get('originCountry') : NULL;
        if(!empty($originCountry)){
            $recipeInfo->setOriginCountry($originCountry);
        }


        $recipeCuisine = !empty($request->get('recipeCuisine')) ? $request->get('recipeCuisine') : NULL;
        if(!empty($recipeCuisine)){
            $recipeInfo->setRecipeCuisine($recipeCuisine);
        }
        $inYourBox = !empty($request->get('inYourBox')) ? $request->get('inYourBox') : NULL;
        if(!empty($inYourBox)){
            $recipeInfo->setInYourBox($inYourBox);
        }
        $goustoReference = !empty($request->get('goustoReference')) ? $request->get('goustoReference') : NULL;
        if(!empty($goustoReference)){
            $recipeInfo->setGoustoReference($goustoReference);
        }
        $rate = !empty($request->get('rate')) ? $request->get('rate') : NULL;
        if(!empty($rate) && ($rate < 1 || $rate > 5)){
            throw new HttpException(400, "Rate Value {$rate} has to be between 1 and 5");
        }
        if(!empty($rate)){
            $recipeInfo->setRate($rate);
        }


        $recipeService->saveRecipe($recipeInfo);
        return $recipeInfo;


    }

    /**
     * Create recipe
     *
     * @ApiDoc(
     *  section = "Recipe",
     *  resource = true,
     *  description="Create recipe ",
     *  requirements={
     *      {
     *          "name"="boxType",
     *          "dataType"="string"
     *      },
     *     {
     *          "name"="title",
     *          "dataType"="string"
     *      },
     *     {
     *          "name"="slug",
     *          "dataType"="string"
     *      },
     *     {
     *          "name"="shortTitle",
     *          "dataType"="string"
     *      },
     *     {
     *          "name"="marketingDescription",
     *          "dataType"="string"
     *      },
     *
     *
     *     {
     *          "name"="caloriesKcal",
     *          "dataType"="integer"
     *      },
     *     {
     *          "name"="proteinGrams",
     *          "dataType"="integer"
     *      },
     *     {
     *          "name"="fatGrams",
     *          "dataType"="integer"
     *      },
     *     {
     *          "name"="carbsGrams",
     *          "dataType"="integer"
     *      },
     *     {
     *          "name"="bulletpoint1",
     *          "dataType"="string"
     *      },
     *
     *
     *     {
     *          "name"="bulletpoint2",
     *          "dataType"="string"
     *      },
     *     {
     *          "name"="bulletpoint3",
     *          "dataType"="string"
     *      },
     *     {
     *          "name"="recipeDietTypeId",
     *          "dataType"="string"
     *      },
     *     {
     *          "name"="season",
     *          "dataType"="string"
     *      },
     *     {
     *          "name"="base",
     *          "dataType"="string"
     *      },
     *
     *     {
     *          "name"="proteinSource",
     *          "dataType"="string"
     *      },
     *     {
     *          "name"="preparationTimeMinutes",
     *          "dataType"="integer"
     *      },
     *     {
     *          "name"="shelfLifeDays",
     *          "dataType"="integer"
     *      },
     *     {
     *          "name"="equipmentNeeded",
     *          "dataType"="string"
     *      },
     *     {
     *          "name"="originCountry",
     *          "dataType"="string"
     *      },
     *
     *     {
     *          "name"="recipeCuisine",
     *          "dataType"="string"
     *      },
     *     {
     *          "name"="inYourBox",
     *          "dataType"="string"
     *      },
     *     {
     *          "name"="goustoReference",
     *          "dataType"="integer"
     *      },
     *     {
     *          "name"="rate",
     *          "dataType"="integer"
     *      }
     *
     *  }
     * )
     */
    public function postRecipeAction(Request $request){
        $recipeService = $this->get('recipe.service');
        $recipeInfo = new Recipe();

        $boxType = !empty($request->get('boxType')) ? $request->get('boxType') : NULL;
        if(!empty($boxType)){
            $recipeInfo->setBoxType($boxType);
        }
        $title = !empty($request->get('title')) ? $request->get('title') : NULL;
        if(!empty($title)){
            $recipeInfo->setTitle($title);
        }
        $slug = !empty($request->get('slug')) ? $request->get('slug') : NULL;
        if(!empty($slug)){
            $recipeInfo->setSlug($slug);
        }
        $shortTitle = !empty($request->get('shortTitle')) ? $request->get('shortTitle') : NULL;
        if(!empty($shortTitle)){
            $recipeInfo->setShortTitle($shortTitle);
        }
        $marketingDescription = !empty($request->get('marketingDescription')) ? $request->get('marketingDescription') : NULL;
        if(!empty($marketingDescription)){
            $recipeInfo->setMarketingDescription($marketingDescription);
        }


        $caloriesKcal = !empty($request->get('caloriesKcal')) ? $request->get('caloriesKcal') : NULL;
        if(!empty($caloriesKcal)){
            $recipeInfo->setCaloriesKcal($caloriesKcal);
        }
        $proteinGrams = !empty($request->get('proteinGrams')) ? $request->get('proteinGrams') : NULL;
        if(!empty($proteinGrams)){
            $recipeInfo->setProteinGrams($proteinGrams);
        }
        $fatGrams = !empty($request->get('fatGrams')) ? $request->get('fatGrams') : NULL;
        if(!empty($fatGrams)){
            $recipeInfo->setFatGrams($fatGrams);
        }
        $carbsGrams = !empty($request->get('carbsGrams')) ? $request->get('carbsGrams') : NULL;
        if(!empty($carbsGrams)){
            $recipeInfo->setCarbsGrams($carbsGrams);
        }
        $bulletpoint1 = !empty($request->get('bulletpoint1')) ? $request->get('bulletpoint1') : NULL;
        if(!empty($slug)){
            $recipeInfo->setBulletpoint1($bulletpoint1);
        }


        $bulletpoint2 = !empty($request->get('bulletpoint2')) ? $request->get('bulletpoint2') : NULL;
        if(!empty($bulletpoint2)){
            $recipeInfo->setBulletpoint2($bulletpoint2);
        }
        $bulletpoint3 = !empty($request->get('bulletpoint3')) ? $request->get('bulletpoint3') : NULL;
        if(!empty($bulletpoint3)){
            $recipeInfo->setBulletpoint3($bulletpoint3);
        }
        $recipeDietTypeId = !empty($request->get('recipeDietTypeId')) ? $request->get('recipeDietTypeId') : NULL;
        if(!empty($recipeDietTypeId)){
            $recipeInfo->setRecipeDietTypeId($recipeDietTypeId);
        }
        $season = !empty($request->get('season')) ? $request->get('season') : NULL;
        if(!empty($season)){
            $recipeInfo->setSeason($season);
        }
        $base = !empty($request->get('base')) ? $request->get('base') : NULL;
        if(!empty($base)){
            $recipeInfo->setBase($base);
        }


        $proteinSource = !empty($request->get('proteinSource')) ? $request->get('proteinSource') : NULL;
        if(!empty($proteinSource)){
            $recipeInfo->setProteinSource($proteinSource);
        }
        $preparationTimeMinutes = !empty($request->get('preparationTimeMinutes')) ? $request->get('preparationTimeMinutes') : NULL;
        if(!empty($preparationTimeMinutes)){
            $recipeInfo->setPreparationTimeMinutes($preparationTimeMinutes);
        }
        $shelfLifeDays = !empty($request->get('shelfLifeDays')) ? $request->get('shelfLifeDays') : NULL;
        if(!empty($shelfLifeDays)){
            $recipeInfo->setShelfLifeDays($shelfLifeDays);
        }
        $equipmentNeeded = !empty($request->get('equipmentNeeded')) ? $request->get('equipmentNeeded') : NULL;
        if(!empty($equipmentNeeded)){
            $recipeInfo->setEquipmentNeeded($equipmentNeeded);
        }
        $originCountry = !empty($request->get('originCountry')) ? $request->get('originCountry') : NULL;
        if(!empty($originCountry)){
            $recipeInfo->setOriginCountry($originCountry);
        }


        $recipeCuisine = !empty($request->get('recipeCuisine')) ? $request->get('recipeCuisine') : NULL;
        if(!empty($recipeCuisine)){
            $recipeInfo->setRecipeCuisine($recipeCuisine);
        }
        $inYourBox = !empty($request->get('inYourBox')) ? $request->get('inYourBox') : NULL;
        if(!empty($inYourBox)){
            $recipeInfo->setInYourBox($inYourBox);
        }
        $goustoReference = !empty($request->get('goustoReference')) ? $request->get('goustoReference') : NULL;
        if(!empty($goustoReference)){
            $recipeInfo->setGoustoReference($goustoReference);
        }
        $rate = !empty($request->get('rate')) ? $request->get('rate') : NULL;
        if(!empty($rate) && ($rate < 1 || $rate > 5)){
            throw new HttpException(400, "Rate Value {$rate} has to be between 1 and 5");
        }
        if(!empty($rate)){
            $recipeInfo->setRate($rate);
        }


        $recipeService->saveRecipe($recipeInfo);
        return $recipeInfo;


    }
}
