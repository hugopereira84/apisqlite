<?php

namespace RecipeBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Recipe
 *
 * @ORM\Table(name="recipe")
 * @ORM\Entity(repositoryClass="RecipeBundle\Repository\RecipeRepository")
 * @ORM\HasLifecycleCallbacks()
 */
class Recipe
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="createdAt", type="datetime")
     */
    private $createdAt;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="updatedAt", type="datetime")
     */
    private $updatedAt;

    /**
     * @var string
     *
     * @ORM\Column(name="box_type", type="string", length=50)
     */
    private $boxType;

    /**
     * @var string
     *
     * @ORM\Column(name="title", type="string", length=255)
     */
    private $title;

    /**
     * @var string
     *
     * @ORM\Column(name="slug", type="string", length=255)
     */
    private $slug;

    /**
     * @var string
     *
     * @ORM\Column(name="short_title", type="string", length=150, nullable=true)
     */
    private $shortTitle;

    /**
     * @var string
     *
     * @ORM\Column(name="marketing_description", type="text")
     */
    private $marketingDescription;

    /**
     * @var int
     *
     * @ORM\Column(name="calories_kcal", type="integer")
     */
    private $caloriesKcal;

    /**
     * @var int
     *
     * @ORM\Column(name="protein_grams", type="integer")
     */
    private $proteinGrams;

    /**
     * @var int
     *
     * @ORM\Column(name="fat_grams", type="integer")
     */
    private $fatGrams;

    /**
     * @var int
     *
     * @ORM\Column(name="carbs_grams", type="integer")
     */
    private $carbsGrams;

    /**
     * @var string
     *
     * @ORM\Column(name="bulletpoint1", type="string", length=255, nullable=true)
     */
    private $bulletpoint1;

    /**
     * @var string
     *
     * @ORM\Column(name="bulletpoint2", type="string", length=255, nullable=true)
     */
    private $bulletpoint2;

    /**
     * @var string
     *
     * @ORM\Column(name="bulletpoint3", type="string", length=255, nullable=true)
     */
    private $bulletpoint3;

    /**
     * @var string
     *
     * @ORM\Column(name="recipe_diet_type_id", type="string", length=100)
     */
    private $recipeDietTypeId;

    /**
     * @var string
     *
     * @ORM\Column(name="season", type="string", length=100)
     */
    private $season;

    /**
     * @var string
     *
     * @ORM\Column(name="base", type="string", length=150, nullable=true)
     */
    private $base;

    /**
     * @var string
     *
     * @ORM\Column(name="protein_source", type="string", length=150)
     */
    private $proteinSource;

    /**
     * @var int
     *
     * @ORM\Column(name="preparation_time_minutes", type="integer")
     */
    private $preparationTimeMinutes;

    /**
     * @var int
     *
     * @ORM\Column(name="shelf_life_days", type="integer")
     */
    private $shelfLifeDays;

    /**
     * @var string
     *
     * @ORM\Column(name="equipment_needed", type="string", length=150)
     */
    private $equipmentNeeded;

    /**
     * @var string
     *
     * @ORM\Column(name="origin_country", type="string", length=150)
     */
    private $originCountry;

    /**
     * @var string
     *
     * @ORM\Column(name="recipe_cuisine", type="string", length=100)
     */
    private $recipeCuisine;

    /**
     * @var string
     *
     * @ORM\Column(name="in_your_box", type="text", nullable=true)
     */
    private $inYourBox;

    /**
     * @var int
     *
     * @ORM\Column(name="gousto_reference", type="integer")
     */
    private $goustoReference;

    /**
     * @var int
     *
     * @ORM\Column(name="rate", type="integer", nullable=true))
     * @Assert\Range(
     *      min = 1,
     *      max = 5,
     *      minMessage = "Your rate has to be bigger then {{ limit }}",
     *      maxMessage = "Your rate has to be lower then {{ limit }}"
     * )
     */
     private $rate;


    /**
     * @ORM\PrePersist
     * @ORM\PreUpdate
     */
    public function updateTrail() {
        $this->setUpdatedAt(new \DateTime());

        if ($this->getCreatedAt() == null)
        {
            $this->setCreatedAt(new \DateTime());
        }
    }

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }


    /**
     * Get createdAt
     *
     * @return \DateTime
     */
    public function getCreatedAt() {
        return $this->createdAt;
    }


    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     *
     * @return Creative
     */
    public function setCreatedAt($createdAt) {
        $this->createdAt = $createdAt;

        return $this;
    }


    /**
     * Get updatedAt
     *
     * @return \DateTime
     */
    public function getUpdatedAt() {
        return $this->updatedAt;
    }


    /**
     * Set updatedAt
     *
     * @param \DateTime $updatedAt
     *
     * @return Creative
     */
    public function setUpdatedAt($updatedAt) {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * Set boxType
     *
     * @param string $boxType
     *
     * @return Recipe
     */
    public function setBoxType($boxType)
    {
        $this->boxType = $boxType;

        return $this;
    }

    /**
     * Get boxType
     *
     * @return string
     */
    public function getBoxType()
    {
        return $this->boxType;
    }

    /**
     * Set title
     *
     * @param string $title
     *
     * @return Recipe
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set slug
     *
     * @param string $slug
     *
     * @return Recipe
     */
    public function setSlug($slug)
    {
        $this->slug = $slug;

        return $this;
    }

    /**
     * Get slug
     *
     * @return string
     */
    public function getSlug()
    {
        return $this->slug;
    }

    /**
     * Set shortTitle
     *
     * @param string $shortTitle
     *
     * @return Recipe
     */
    public function setShortTitle($shortTitle)
    {
        $this->shortTitle = $shortTitle;

        return $this;
    }

    /**
     * Get shortTitle
     *
     * @return string
     */
    public function getShortTitle()
    {
        return $this->shortTitle;
    }

    /**
     * Set marketingDescription
     *
     * @param string $marketingDescription
     *
     * @return Recipe
     */
    public function setMarketingDescription($marketingDescription)
    {
        $this->marketingDescription = $marketingDescription;

        return $this;
    }

    /**
     * Get marketingDescription
     *
     * @return string
     */
    public function getMarketingDescription()
    {
        return $this->marketingDescription;
    }

    /**
     * Set caloriesKcal
     *
     * @param integer $caloriesKcal
     *
     * @return Recipe
     */
    public function setCaloriesKcal($caloriesKcal)
    {
        $this->caloriesKcal = $caloriesKcal;

        return $this;
    }

    /**
     * Get caloriesKcal
     *
     * @return int
     */
    public function getCaloriesKcal()
    {
        return $this->caloriesKcal;
    }

    /**
     * Set proteinGrams
     *
     * @param integer $proteinGrams
     *
     * @return Recipe
     */
    public function setProteinGrams($proteinGrams)
    {
        $this->proteinGrams = $proteinGrams;

        return $this;
    }

    /**
     * Get proteinGrams
     *
     * @return int
     */
    public function getProteinGrams()
    {
        return $this->proteinGrams;
    }

    /**
     * Set fatGrams
     *
     * @param integer $fatGrams
     *
     * @return Recipe
     */
    public function setFatGrams($fatGrams)
    {
        $this->fatGrams = $fatGrams;

        return $this;
    }

    /**
     * Get fatGrams
     *
     * @return int
     */
    public function getFatGrams()
    {
        return $this->fatGrams;
    }

    /**
     * Set carbsGrams
     *
     * @param integer $carbsGrams
     *
     * @return Recipe
     */
    public function setCarbsGrams($carbsGrams)
    {
        $this->carbsGrams = $carbsGrams;

        return $this;
    }

    /**
     * Get carbsGrams
     *
     * @return integer
     */
    public function getCarbsGrams()
    {
        return $this->carbsGrams;
    }

    /**
     * Set bulletpoint1
     *
     * @param string $bulletpoint1
     *
     * @return Recipe
     */
    public function setBulletpoint1($bulletpoint1)
    {
        $this->bulletpoint1 = $bulletpoint1;

        return $this;
    }

    /**
     * Get bulletpoint1
     *
     * @return string
     */
    public function getBulletpoint1()
    {
        return $this->bulletpoint1;
    }

    /**
     * Set bulletpoint2
     *
     * @param string $bulletpoint2
     *
     * @return Recipe
     */
    public function setBulletpoint2($bulletpoint2)
    {
        $this->bulletpoint2 = $bulletpoint2;

        return $this;
    }

    /**
     * Get bulletpoint2
     *
     * @return string
     */
    public function getBulletpoint2()
    {
        return $this->bulletpoint2;
    }

    /**
     * Set bulletpoint3
     *
     * @param string $bulletpoint3
     *
     * @return Recipe
     */
    public function setBulletpoint3($bulletpoint3)
    {
        $this->bulletpoint3 = $bulletpoint3;

        return $this;
    }

    /**
     * Get bulletpoint3
     *
     * @return string
     */
    public function getBulletpoint3()
    {
        return $this->bulletpoint3;
    }

    /**
     * Set recipeDietTypeId
     *
     * @param string $recipeDietTypeId
     *
     * @return Recipe
     */
    public function setRecipeDietTypeId($recipeDietTypeId)
    {
        $this->recipeDietTypeId = $recipeDietTypeId;

        return $this;
    }

    /**
     * Get recipeDietTypeId
     *
     * @return string
     */
    public function getRecipeDietTypeId()
    {
        return $this->recipeDietTypeId;
    }

    /**
     * Set season
     *
     * @param string $season
     *
     * @return Recipe
     */
    public function setSeason($season)
    {
        $this->season = $season;

        return $this;
    }

    /**
     * Get season
     *
     * @return string
     */
    public function getSeason()
    {
        return $this->season;
    }

    /**
     * Set base
     *
     * @param string $base
     *
     * @return Recipe
     */
    public function setBase($base)
    {
        $this->base = $base;

        return $this;
    }

    /**
     * Get base
     *
     * @return string
     */
    public function getBase()
    {
        return $this->base;
    }

    /**
     * Set proteinSource
     *
     * @param string $proteinSource
     *
     * @return Recipe
     */
    public function setProteinSource($proteinSource)
    {
        $this->proteinSource = $proteinSource;

        return $this;
    }

    /**
     * Get proteinSource
     *
     * @return string
     */
    public function getProteinSource()
    {
        return $this->proteinSource;
    }

    /**
     * Set preparationTimeMinutes
     *
     * @param integer $preparationTimeMinutes
     *
     * @return Recipe
     */
    public function setPreparationTimeMinutes($preparationTimeMinutes)
    {
        $this->preparationTimeMinutes = $preparationTimeMinutes;

        return $this;
    }

    /**
     * Get preparationTimeMinutes
     *
     * @return int
     */
    public function getPreparationTimeMinutes()
    {
        return $this->preparationTimeMinutes;
    }

    /**
     * Set shelfLifeDays
     *
     * @param integer $shelfLifeDays
     *
     * @return Recipe
     */
    public function setShelfLifeDays($shelfLifeDays)
    {
        $this->shelfLifeDays = $shelfLifeDays;

        return $this;
    }

    /**
     * Get shelfLifeDays
     *
     * @return int
     */
    public function getShelfLifeDays()
    {
        return $this->shelfLifeDays;
    }

    /**
     * Set equipmentNeeded
     *
     * @param string $equipmentNeeded
     *
     * @return Recipe
     */
    public function setEquipmentNeeded($equipmentNeeded)
    {
        $this->equipmentNeeded = $equipmentNeeded;

        return $this;
    }

    /**
     * Get equipmentNeeded
     *
     * @return string
     */
    public function getEquipmentNeeded()
    {
        return $this->equipmentNeeded;
    }

    /**
     * Set originCountry
     *
     * @param string $originCountry
     *
     * @return Recipe
     */
    public function setOriginCountry($originCountry)
    {
        $this->originCountry = $originCountry;

        return $this;
    }

    /**
     * Get originCountry
     *
     * @return string
     */
    public function getOriginCountry()
    {
        return $this->originCountry;
    }

    /**
     * Set recipeCuisine
     *
     * @param string $recipeCuisine
     *
     * @return Recipe
     */
    public function setRecipeCuisine($recipeCuisine)
    {
        $this->recipeCuisine = $recipeCuisine;

        return $this;
    }

    /**
     * Get recipeCuisine
     *
     * @return string
     */
    public function getRecipeCuisine()
    {
        return $this->recipeCuisine;
    }

    /**
     * Set inYourBox
     *
     * @param string $inYourBox
     *
     * @return Recipe
     */
    public function setInYourBox($inYourBox)
    {
        $this->inYourBox = $inYourBox;

        return $this;
    }

    /**
     * Get inYourBox
     *
     * @return string
     */
    public function getInYourBox()
    {
        return $this->inYourBox;
    }

    /**
     * Set goustoReference
     *
     * @param integer $goustoReference
     *
     * @return Recipe
     */
    public function setGoustoReference($goustoReference)
    {
        $this->goustoReference = $goustoReference;

        return $this;
    }

    /**
     * Get goustoReference
     *
     * @return int
     */
    public function getGoustoReference()
    {
        return $this->goustoReference;
    }

    /**
     * Set rate
     *
     * @param integer $rate
     *
     * @return Recipe
     */
    public function setRate($rate)
    {
        $this->rate = $rate;

        return $this;
    }

    /**
     * Get $rate
     *
     * @return int
     */
    public function getRate()
    {
        return $this->rate;
    }
}

