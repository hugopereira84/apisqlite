<?php

namespace RecipeBundle\Service;

use Symfony\Component\HttpKernel\Bundle\Bundle;
use RecipeBundle\Entity\Recipe;
use Doctrine\ORM\EntityManagerInterface;

class RecipeService
{
    /**
     * @param EntityManagerInterface $entityManager
     */
    public function __construct(EntityManagerInterface $entityManager) {
        $this->em = $entityManager;
    }

    /**
     * Returns absctract query, to create pagination
     *
     * @return \Doctrine\ORM\AbstractQuery
     */
    public function getRecipePaginatorQuery($extraParams) {
        $queryBuilder = $this->getRecipeQueryBuilder();

        if( !empty($extraParams) && !empty($extraParams["findBy"]) ){
            foreach($extraParams["findBy"] as $paramName => $paramValue){
                $queryBuilder->andWhere('r.'.$paramName.' = :search')
                             ->setParameter('search', $paramValue);
            }

        }

        $countQb  = clone $queryBuilder;
        $selectQb = clone $queryBuilder;


        $countQuery       = $countQb->select("COUNT (r)")->getQuery()->getSingleScalarResult();
        $query = $selectQb->getQuery()->setHint('knp_paginator.count', $countQuery);

        return $query;
    }

    /**
     * Returns query builder
     *
     * @return \Doctrine\ORM\QueryBuilder
     */
    public function getRecipeQueryBuilder() {
        return $this->em->getRepository('RecipeBundle:Recipe')->createQueryBuilder('r');
    }

    /**
     * Get recipe by id
     *
     * @param $id
     * @return mixed
     */
    public function getById($id){
        return $this->em->getRepository('RecipeBundle:Recipe')->find($id);
    }
    /**
     * Save Recipe
     *
     * @param Recipe $recipe
     * @return Recipe
     */
    public function saveRecipe(Recipe $recipe) {
        $this->em->persist($recipe);
        $this->em->flush();

        return $recipe;
    }

    /**
     * Delete Recipe
     *
     * @param Recipe $recipe
     * @return mixed
     */
    public function deleteRecipe($recipe) {
        $this->em->remove($recipe);
        $this->em->flush();

        return $recipe;
    }
}
