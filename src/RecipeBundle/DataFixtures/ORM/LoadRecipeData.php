<?php

namespace RecipeBundle\Bundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use RecipeBundle\Entity\Recipe;

class LoadUserData implements FixtureInterface
{
    /**
     * data seed: php bin/console doctrine:fixtures:load
     * @param ObjectManager $manager
     */
    public function load(ObjectManager $manager)
    {
        $allInfo = array(
            array(
                "30/06/2015 17:58:00", 	"30/06/2015 17:58:00",
                "vegetarian", "Sweet Chilli and Lime Beef on a Crunchy Fresh Noodle Salad",
                "sweet-chilli-and-lime-beef-on-a-crunchy-fresh-noodle-salad", NULL,
                "Here we've used onglet steak which is an extra flavoursome cut of beef that should never be cooked past medium rare. So if you're a fan of well done steak, this one may not be for you. However, if you love rare steak and fancy trying a new cut, please be",
                401, 12, 35, 0,
                NULL,NULL,NULL,
                "meat", "all", "noodles",
                "beef",	35,	4,	"Appetite",
                "Great Britain", "asian", NULL,
                59
            ),

            array(
                "30/06/2015 17:58:00", 	"30/06/2015 17:58:00",
                "gourmet", "Tamil Nadu Prawn Masala",
                "tamil-nadu-prawn-masala", NULL,
                "Tamil Nadu is a state on the eastern coast of the southern tip of India. Curry from there is particularly famous and it's easy to see why. This one is brimming with exciting contrasting tastes from ingredients like chilli powder, coriander and fennel seed",
                524, 12, 22, 0,
                "Vibrant & Fresh",	"Warming, not spicy", "Curry From Scratch",
                "fish", "all", "pasta",
                "seafood", 40, 4, "Appetite",
                "Great Britain", "italian" , "king prawns, basmati rice, onion, tomatoes, garlic, ginger, ground tumeric, red chilli powder, ground cumin, fresh coriander, curry leaves, fennel seeds",
                58
            ),
            array(
                "30/06/2015 17:58:00",	"30/06/2015 17:58:00",
                "vegetarian",	"Umbrian Wild Boar Salami Ragu with Linguine",
                "umbrian-wild-boar-salami-ragu-with-linguine", NULL,
                "This delicious pasta dish comes from the Italian region of Umbria. It has a smoky and intense wild boar flavour which combines the earthy garlic, leek and onion flavours, while the chilli flakes add a nice deep aroma. Enjoy within 5-6 days of delivery.",
                609,17,29,0,
                NULL,NULL,NULL,
                "meat", "all", "pasta",
                "pork", 35,	4, "Appetite",
                "Great Britain", "british", NULL,
                1
            ),
            //id=4
            array(
                "30/06/2015 17:58:00",	"30/06/2015 17:58:00",
                "gourmet", "Tenderstem and Portobello Mushrooms with Corn Polenta",
                "tenderstem-and-portobello-mushrooms-with-corn-polenta", NULL,
                "One for those who like their veggies with a slightly spicy kick. However, those short on time, be warned ' this is a time-consuming dish, but if you're willing to spend a few extra minutes in the kitchen, the fresh corn mash is extraordinary and worth a t",
                508, 28, 20, 0,
                NULL,NULL,NULL,
                "vegetarian", "all", NULL,
                "cheese", 50,	4, 	"None",
                "Great Britain", "british", NULL,
                56
            ),
            //id=5
            array(
                "30/06/2015 17:58:00", "30/06/2015 17:58:00",
                "vegetarian", "Fennel Crusted Pork with Italian Butter Beans",
                "fennel-crusted-pork-with-italian-butter-beans", NULL,
                "A classic roast with a twist. The pork loin is marinated in rosemary, fennel seeds and chilli flakes then teamed with baked potato wedges and butter beans in tomato sauce. Enjoy within 5-6 days of delivery.",
                511, 11, 62, 0,
                "A roast with a twist", "Low fat & high protein", "With roast potatoes",
                "meat", "all", "beans/lentils",
                "pork", 45, 4, "Pestle & Mortar (optional)",
                "Great Britain", "british",	"pork tenderloin, potatoes, butter beans, garlic, fennel seeds, medium onion, chilli flakes, fresh rosemary, tomatoes, vegetable stock cube",
                55
            ),
            //id=6
            array(
                "01/07/2015 17:58:00", "01/07/2015 17:58:00",
                "gourmet", "Pork Chilli",
                "pork-chilli", NULL,
                "Succulent pork tenderloin and feathery white bean and parsnip mash mingle with feisty cumin seeds and tangy leek in this lighter, less conventional take on a British classic. Welcome to the outer limits of food!",
                401, 12, 35, 0,
                NULL,NULL,NULL,
                "meat", "all", NULL,
                "pork", 35, 4, "Appetite",
                "Great Britain", "asian", NULL,
                60
            ),
            //id=7
            array(
                "02/07/2015 17:58:00", 	"02/07/2015 17:58:00",
                "vegetarian", "Courgette Pasta Rags",
                "courgette-pasta-rags", NULL,
                "Kick-start the new year with some get-up and go with this lean green vitality machine. Protein-packed chicken and mineral-rich kale are blended into a smooth, nut-free version of pesto; creating the ultimate composition of nutrition and taste",
                524,12,22,0,
                NULL,NULL,NULL,
                "meat", "all", NULL,
                "chicken", 40, 4, "Appetite",
                "Great Britain", "british", NULL,
                59
            ),
            //id=8
            array(
                "03/07/2015 17:58:00", "03/07/2015 17:58:00",
                "vegetarian", "Homemade Eggs & Beans",
                "homemade-egg-beans", NULL,
                "A Goustofied British institution, learn how to make beautifully golden breaded chicken escalopes drizzled in homemade garlic butter and served atop fluffy potato and broccoli mash.",
                609, 17, 29, 0,
                NULL,NULL,NULL,
                "meat", "all", NULL,
                "eggs",	35, 3, "Appetite",
                "Great Britain", "italian", NULL,
                2
            ),
            //id=9
            array(
                "04/07/2015 17:58:00", "04/07/2015 17:58:00",
                "gourmet",	"Grilled Jerusalem Fish",
                "grilled-jerusalem-fish", NULL,
                "I love this super healthy fish dish, it contains a punch from zingy ginger, a kick from chili and a salty sweet balance from soy sauce and mirim. A cleansing and restorative meal, great for body and soul.",
                508, 28, 20, 0,
                NULL,NULL,NULL,
                "meat", "all", NULL,
                "fish", 50, 4, "Appetite",
                "Great Britain", "mediterranean", NULL,
                57
            ),
            //id=10
            array(
                "05/07/2015 17:58:00", "05/07/2015 17:58:00",
                "gourmet", "Pork Katsu Curry",
                "pork-katsu-curry", NULL,
                "Comprising all the best bits of the classic American number and none of the mayo, this is a warm & tasty chicken and bulgur salad with just a hint of Scandi influence. A beautifully summery medley of flavours and textures",
                511, 11, 62, 0,
                NULL,NULL,NULL,
                "meat", "all", NULL,
                "pork", 45, 4, "Appetite",
                "Great Britain", "mexican", NULL,
                56

            )
        );



        foreach($allInfo as $info){
            $createdAt = \DateTime::createFromFormat('d/m/Y H:i:s', $info[0]);
            $updatedAt = \DateTime::createFromFormat('d/m/Y H:i:s', $info[1]);

            $recipe = new Recipe();
            $recipe->setCreatedAt($createdAt);
            $recipe->setUpdatedAt($updatedAt);
            $recipe->setBoxType($info[2]);
            $recipe->setTitle($info[3]);
            $recipe->setSlug($info[4]);
            $recipe->setShortTitle($info[5]);
            $recipe->setMarketingDescription($info[6]);
            $recipe->setCaloriesKcal($info[7]);
            $recipe->setProteinGrams($info[8]);
            $recipe->setFatGrams($info[9]);
            $recipe->setCarbsGrams($info[10]);
            $recipe->setBulletpoint1($info[11]);
            $recipe->setBulletpoint2($info[12]);
            $recipe->setBulletpoint3($info[13]);
            $recipe->setRecipeDietTypeId($info[14]);
            $recipe->setSeason($info[15]);
            $recipe->setBase($info[16]);
            $recipe->setProteinSource($info[17]);
            $recipe->setPreparationTimeMinutes($info[18]);
            $recipe->setShelfLifeDays($info[19]);
            $recipe->setEquipmentNeeded($info[20]);
            $recipe->setOriginCountry($info[21]);
            $recipe->setRecipeCuisine($info[22]);
            $recipe->setInYourBox($info[23]);
            $recipe->setGoustoReference($info[24]);

            $manager->persist($recipe);
            $manager->flush();
        }


    }
}